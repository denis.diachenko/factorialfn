const factorial = (number) => {
  if (number < 0) {
    throw new Error("Use positive numbers only!");
  }
  return number ? number * factorial(number - 1) : 1;
};
export { factorial };
